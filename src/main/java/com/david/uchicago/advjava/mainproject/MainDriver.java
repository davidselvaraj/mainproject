/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.david.uchicago.advjava.mainproject;

import com.david.uchicago.advjava.mystringutils.StringUtil;

/**
 *
 * @author davids
 */
public class MainDriver {
    
    public static void main(String[] args) {
        String name = "   ";
        
        if(StringUtil.isEmpty(name)) {
            System.out.println("Name is empty");
        } else {
            System.out.println("Name is not empty");
        }
    }
    
}
